#include <iostream>
#include <vector>
using namespace std;

void print(auto A)
{
   for (auto a : A) 
        cout <<a<<" ";

   cout<<endl;
}


void mystery1(auto& Data)
{
  cout<<endl<<"Mystery 1"<<endl<<"---------------------"<<endl;

  for ( int i = 0 ; i < Data.size( ) ; i++)
  {
    for ( int j = 0 ; j < i ; j++)
	if ( Data[ i ] < Data[ j ] )
	    swap( Data[ i ] , Data[ j ] );

    print(Data);
  }//end outer for (this brace is needed to include the print statement)

}

void mystery2(auto& Data)
{
	cout<<endl<<"Mystery2"<<endl<<"----------------"<<endl;
	for(int a =0; a < Data.size (); a++)
		{
		for (int b =0; b < Data.size () -1; b++)
			{ if (Data[b] > Data [b+1])
				swap( Data [b],Data [b +1] );
				
			}
			print(Data);
		}
}

void mystery3 (auto & Data)
	{
		cout<<endl<<"Mystery3"<<endl<<"-------------"<<endl;
	
	for(int c = 0; c < Data.size (); c++)
	{
		int min = c;
		for (int k= c+1; k < Data.size(); k++)
		{
			if(Data[k]< Data[min])
			min = k;
		}//end inner for
		swap(Data [c],Data[min]);
		print (Data);
	}//end outer for
	
	}//end of function	
		
		

	
int main()
{
    
  vector<int> Data = {36, 18, 22, 30, 29, 25, 12};

  vector<int> D1 = Data;
  vector<int> D2 = Data;
  vector<int> D3 = Data;

  mystery1(D1);
  mystery2(D2);
  mystery3(D3);

}
